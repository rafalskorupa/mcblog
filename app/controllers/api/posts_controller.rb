# frozen_string_literal: true

module Api
  class PostsController < ApplicationController
    include Responses

    def index
      ok!(representer.many(blog_repository.all_posts))
    end

    def show
      ok!(representer.one(blog_repository.one_post(params[:id])))
    end

    def create
      return created!(representer.one(add_post.result)) if add_post.success?

      bad_request!(add_post.errors)
    end

    def report
      @post = blog_repository.one_post(params[:id])

      render(
        xlsx: "post_#{@post.id}",
        template: 'blog/posts/single_post.xlsx.axlsx'
      )
    end

    def global_report
      @posts = blog_repository.all_posts
      @comments = blog_repository.all_comments

      render(
        xlsx: 'global', template: 'blog/posts/global.xlsx.axlsx'
      )
    end

    private

    def add_post
      @add_post ||= Blog::UseCase::AddPost.call(post_params.deep_symbolize_keys)
    end

    def blog_repository
      Blog::Repository
    end

    def post_params
      params.require(:post).permit(:title, :author, :content)
    end

    def representer
      PostRepresenter
    end
  end
end
