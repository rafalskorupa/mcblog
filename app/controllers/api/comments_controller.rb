# frozen_string_literal: true

module Api
  class CommentsController < ApplicationController
    include Responses

    def create
      bad_request!(add_comment.errors) unless add_comment.success?

      created!(representer.one(add_comment.result))
    end

    private

    def add_comment
      @add_comment = Blog::UseCase::AddComment.call(comment_params)
    end

    def blog_repository
      Blog::Repository
    end

    def comment_params
      params
        .require(:comment)
        .permit(:post_id, :author, :content, :comment_id)
        .deep_symbolize_keys
    end

    def representer
      CommentRepresenter
    end
  end
end
