# frozen_string_literal: true

module Responses
  extend ActiveSupport::Concern
  def ok!(data)
    render json: { data: data }, status: :ok
  end

  def created!(data)
    render json: { success: true, data: data }, status: :created
  end

  def bad_request!(errors)
    render json: { success: false, errors: errors }, status: :bad_request
  end
end
