# frozen_string_literal: true

module Blog
  class Post < ApplicationRecord
    has_many(
      :comments,
      class_name: '::Blog::PostComment',
      foreign_key: 'blog_post_id'
    )

    validates :title, presence: true
    validates :content, presence: true
    validates :author, presence: true

    def self.table_name_prefix
      'blog_'
    end
  end
end
