# frozen_string_literal: true

module Blog
  class PostComment < ApplicationRecord
    belongs_to :post, foreign_key: 'blog_post_id', class_name: '::Blog::Post'
    belongs_to(
      :parent_comment,
      foreign_key: 'blog_post_comment_id',
      optional: true
    )

    validates :content, presence: true
    validates :author, presence: true

    def self.table_name_prefix
      'blog_'
    end
  end
end
