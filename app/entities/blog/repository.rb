# frozen_string_literal: true

module Blog
  class Repository
    class << self
      def all_posts
        Blog::Post.all
      end

      def all_comments
        Blog::PostComment.all
      end

      def one_post(post_id)
        Blog::Post.preload(:comments).find(post_id)
      rescue ActiveRecord::RecordNotFound => e
        raise ::Repository::Error, e.message
      end

      def create_post(title:, content:, author:)
        Blog::Post.create!(
          title: title,
          content: content,
          author: author
        )
      rescue ActiveRecord::RecordInvalid, ActiveRecord::InvalidForeignKey => e
        raise ::Repository::Error, e.message
      end

      def create_comment(post_id:, content:, author:, comment_id: nil)
        Blog::PostComment.create!(
          blog_post_id: post_id,
          blog_post_comment_id: comment_id,
          content: content,
          author: author
        )
      rescue ActiveRecord::RecordInvalid, ActiveRecord::InvalidForeignKey => e
        raise ::Repository::Error, e.message
      end
    end
  end
end
