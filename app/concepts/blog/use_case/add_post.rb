# frozen_string_literal: true

module Blog
  module UseCase
    class AddPost
      def self.call(params)
        new(params).tap(&:call)
      end

      def initialize(params)
        @params = params
        @errors = []
      end

      def call
        @result = Blog::Repository.create_post(params)
      rescue ::Repository::Error => e
        errors.append(e.message)
      end

      def success?
        errors.empty?
      end

      attr_reader :errors, :result, :params

      private

      attr_writer :errors
    end
  end
end
