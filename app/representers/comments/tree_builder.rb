# frozen_string_literal: true

module Comments
  class TreeBuilder
    def self.call(comments)
      new(comments).call
    end

    def initialize(comments)
      @comments = comments
    end

    def grouped_comments
      @grouped_comments ||= comments.group_by { |comment| comment[:parent_id] }
    end

    def call
      build_tree(grouped_comments[nil])
    end

    def build_tree(step)
      return [] if step.blank?

      step.map do |comment|
        comment.merge(comments: build_tree(grouped_comments[comment[:id]]))
      end
    end

    private

    attr_reader :comments
  end
end
