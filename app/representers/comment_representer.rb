# frozen_string_literal: true

class CommentRepresenter
  def self.one(comment)
    new(comment).simple
  end

  def self.tree(comments)
    Comments::TreeBuilder.call(many(comments))
  end

  def self.many(comments)
    comments.map { |comment| new(comment).simple }
  end

  def initialize(comment)
    @comment = comment
  end

  def simple
    {
      id: comment.id,
      author: comment.author,
      content: comment.content,
      post_id: comment.blog_post_id,
      parent_id: comment.blog_post_comment_id,
      created_at: comment.created_at
    }
  end

  private

  attr_reader :comment
end
