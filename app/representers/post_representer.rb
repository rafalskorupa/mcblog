# frozen_string_literal: true

class PostRepresenter
  def self.one(post)
    new(post).with_comments
  end

  def self.many(posts)
    posts.map { |post| new(post).simple }
  end

  def initialize(post)
    @post = post
  end

  def simple
    {
      id: post.id,
      author: post.author,
      title: post.title,
      content: post.content
    }
  end

  def with_comments
    {
      id: post.id,
      author: post.author,
      title: post.title,
      content: post.content,
      comments: CommentRepresenter.tree(post.comments)
    }
  end

  private

  attr_reader :post
end
