# frozen_string_literal: true

RSpec.describe 'GET /api/posts', type: :request do
  subject(:api_call) { get('/api/posts') }

  let(:posts) { create_list(:blog_post, 3) }

  context 'when there are some posts' do
    before { posts }

    let(:expected_posts) do
      posts.map do |post|
        {
          id: post.id,
          title: post.title,
          content: post.content,
          author: post.author
        }
      end
    end

    it 'return all posts' do
      api_call
      expect(json_response[:data]).to match_array expected_posts
    end
  end
end
