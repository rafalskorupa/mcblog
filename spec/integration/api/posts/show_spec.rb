# frozen_string_literal: true

RSpec.describe 'GET /api/posts/:id', type: :request do
  subject(:api_call) { get("/api/posts/#{post.id}") }

  let(:post) { create(:blog_post) }

  context 'when post exists' do
    before { post }

    let(:expected_post) do
      {
        id: post.id,
        title: post.title,
        content: post.content,
        author: post.author,
        comments: []
      }
    end

    it 'return all posts' do
      api_call
      expect(json_response[:data]).to match expected_post
    end
  end
end
