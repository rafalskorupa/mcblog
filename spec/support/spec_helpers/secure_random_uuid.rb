# frozen_string_literal: true

module SpecHelpers
  module SecureRandomUuid
    def secure_random_uuid
      SecureRandom.uuid
    end
  end
end

RSpec.configure do |config|
  config.include SpecHelpers::SecureRandomUuid
end
