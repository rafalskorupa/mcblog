# frozen_string_literal: true

module SilencedCall
  def silenced_call
    yield
  rescue StandardError
    nil
  end
end

RSpec.configure do |config|
  config.include(SilencedCall)
end
