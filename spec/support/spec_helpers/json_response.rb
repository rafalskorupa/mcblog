# frozen_string_literal: true

module SpecHelpers
  module JsonResponse
    def json_response
      JSON.parse(response.body).deep_symbolize_keys
    end
  end
end

RSpec.configure do |config|
  config.include SpecHelpers::JsonResponse
end
