# frozen_string_literal: true

FactoryBot.define do
  factory :blog_post, class: Blog::Post do
    sequence :title do |n|
      "title_#{n}"
    end
    content { 'A content of a blog post' }
    author { 'A.A. Milne' }
  end

  factory :blog_comment, class: Blog::PostComment do
    content { 'I am a comment' }
    author { 'Guest' }
    blog_post_id { create(:blog_post) }
  end
end
