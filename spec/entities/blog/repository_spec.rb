# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Blog::Repository do
  subject(:repository) { described_class }

  describe '.all_posts' do
    subject(:all_posts) { repository.all_posts }

    let(:existing_posts) { create_list(:blog_post, 3) }

    it 'return all existing posts' do
      expect(all_posts).to match_array existing_posts
    end
  end

  describe '.one_post' do
    subject(:one_post) { repository.one_post(post_id) }

    context "when post with given id doesn't exist" do
      let(:post_id) { secure_random_uuid }

      it 'raises an error' do
        expect { one_post }.to raise_error Repository::Error
      end
    end

    context 'when post exists' do
      let(:post_id) { post.id }
      let(:post) { create(:blog_post) }

      it 'returns given post' do
        expect(one_post).to match post
      end
    end
  end

  describe '.create_post' do
    subject(:create_post) { repository.create_post(params) }

    let(:params) { attributes_for(:blog_post) }

    it 'creates new post with given attributes' do
      expect(create_post.attributes.deep_symbolize_keys)
        .to match(hash_including(params))
    end

    it 'persists new post in database' do
      expect { create_post }.to change(Blog::Post, :count).by(1)
    end
  end

  describe '.create_comment' do
    subject(:create_comment) { repository.create_comment(params) }
    subject(:post) { create(:blog_post) }

    let(:params) do
      {
        post_id: post_id,
        content: content,
        author: author,
        comment_id: parent_comment_id
      }
    end

    let(:content) { 'Valid content' }
    let(:author) { 'Tom Hardy' }
    let(:parent_comment_id) { nil }

    context 'when adding comment to existing post' do
      let(:post_id) { post.id }

      it 'creates new comment for post' do
        expect { create_comment }.to change(post.comments.reload, :count).by(1)
      end

      context 'when adding to not existing comment' do
        let(:parent_comment_id) { secure_random_uuid }

        it 'raises an error' do
          expect { create_comment }.to raise_error(Repository::Error)
        end

        it "doesn't create new comment" do
          expect { silenced_call { create_comment } }
            .not_to change(Blog::PostComment, :count)
        end
      end
    end

    context 'when adding comment to not-existing post' do
      let(:post_id) { secure_random_uuid }

      it 'raises an error' do
        expect { create_comment }.to raise_error(Repository::Error)
      end

      it "doesn't create new comment" do
        expect { silenced_call { create_comment } }
          .not_to change(Blog::PostComment, :count)
      end
    end
  end
end
