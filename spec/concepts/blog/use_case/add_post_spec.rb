# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Blog::UseCase::AddPost do
  subject(:use_case) { described_class }

  let(:params) { attributes_for(:blog_post) }

  describe 'resolved use_case' do
    subject(:resolved_use_case) { described_class.call(params) }

    context 'when params are valid' do
      let(:new_post) { instance_double(Blog::Post) }

      before do
        allow(Blog::Repository)
          .to receive(:create_post)
          .with(params)
          .and_return(new_post)
      end

      it 'returns new post in #result' do
        expect(resolved_use_case.result).to eql new_post
      end

      it 'returns true in #success?' do
        expect(resolved_use_case.success?).to be true
      end

      it 'returns empty array in #errors' do
        expect(resolved_use_case.errors).to be_empty
      end
    end

    context 'when Repository rejects params' do
      let(:expected_error) { 'repository error' }

      before do
        allow(Blog::Repository)
          .to receive(:create_post)
          .with(params)
          .and_raise ::Repository::Error, expected_error
      end

      it 'returns nil in #result' do
        expect(resolved_use_case.result).to be nil
      end

      it 'returns false in #success?' do
        expect(resolved_use_case.success?).to be false
      end

      it 'returns expected_error in #errors' do
        expect(resolved_use_case.errors).to include expected_error
      end
    end
  end
end
