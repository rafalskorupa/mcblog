# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Blog::UseCase::AddComment do
  subject(:use_case) { described_class }

  let(:params) do
    {
      post_id: secure_random_uuid,
      content: 'content',
      author: 'author'
    }
  end

  describe 'resolved use_case' do
    subject(:resolved_use_case) { described_class.call(params) }

    context 'when params are valid' do
      let(:new_comment) { instance_double(Blog::PostComment) }

      before do
        allow(Blog::Repository)
          .to receive(:create_comment)
          .with(params)
          .and_return(new_comment)
      end

      it 'returns new comment in #result' do
        expect(resolved_use_case.result).to eql new_comment
      end

      it 'returns true in #success?' do
        expect(resolved_use_case.success?).to be true
      end

      it 'returns empty array in #errors' do
        expect(resolved_use_case.errors).to be_empty
      end
    end

    context 'when Repository rejects params' do
      let(:expected_error) { 'repository error' }

      before do
        allow(Blog::Repository)
          .to receive(:create_comment)
          .with(params)
          .and_raise ::Repository::Error, expected_error
      end

      it 'returns nil in #result' do
        expect(resolved_use_case.result).to be nil
      end

      it 'returns false in #success?' do
        expect(resolved_use_case.success?).to be false
      end

      it 'returns expected_error in #errors' do
        expect(resolved_use_case.errors).to include expected_error
      end
    end
  end
end
