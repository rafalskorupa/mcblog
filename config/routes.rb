# frozen_string_literal: true


Rails.application.routes.draw do

  namespace :api do
    resources :posts, only: [:index, :show, :create] do
      collection do
        get :global_report
      end
      member do
        get :report
      end
    end
    resources :comments, only: [:create]
  end
end
