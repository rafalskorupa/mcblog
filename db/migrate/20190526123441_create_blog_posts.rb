class CreateBlogPosts < ActiveRecord::Migration[5.0]
  def change
    create_table :blog_posts, id: :uuid do |t|
      t.string :title
      t.text :content
      t.string :author

      t.timestamps
    end
  end
end
