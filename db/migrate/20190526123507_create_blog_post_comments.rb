class CreateBlogPostComments < ActiveRecord::Migration[5.0]
  def change
    create_table :blog_post_comments, id: :uuid do |t|
      t.references :blog_post, type: :uuid, foreign_key: true, null: false
      t.references :blog_post_comment, type: :uuid, foreign_key: true, null: true
      t.text :content
      t.string :author

      t.timestamps
    end
  end
end
