# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190526123507) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "pgcrypto"
  enable_extension "uuid-ossp"

  create_table "blog_post_comments", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.uuid     "blog_post_id",         null: false
    t.uuid     "blog_post_comment_id"
    t.text     "content"
    t.string   "author"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.index ["blog_post_comment_id"], name: "index_blog_post_comments_on_blog_post_comment_id", using: :btree
    t.index ["blog_post_id"], name: "index_blog_post_comments_on_blog_post_id", using: :btree
  end

  create_table "blog_posts", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.string   "title"
    t.text     "content"
    t.string   "author"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "blog_post_comments", "blog_post_comments"
  add_foreign_key "blog_post_comments", "blog_posts"
end
